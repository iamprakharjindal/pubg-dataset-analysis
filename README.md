# ABOUT
PUBG Project for Machine Learning using Python
This is a project for ML course of Foxmula for making prediction on
given PUBG dataset

## Installation

To run the project 
$ - Download Anaconda Navigator
$ - Launch JupyterLab
$ - Open file "PUBG project (GROUP - ML_007 ).ipynb"

## Team Members

1. Prakhar Jindal
2. Yashraj Hawle